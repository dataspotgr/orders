<?php

return [
    'Welcome'                   =>  'Καλώς Όρισες',
    'Update'                    =>  'Ενημέρωση',
    'Create Client Order'       =>  'Δημιουργία νέας παραγγελίας',
	'Save'                      =>  'Αποθήκευση'
];