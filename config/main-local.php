<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=cucine_orders',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
//        'paypal' => [
//            'class' => 'common\components\Paypal',
//            // Create an app from PayPal and get the following properties
//            // https://developer.paypal.com/docs/integration/admin/manage-apps/
//            'clientId' => '',
//            'clientSecret' => '',
//            'mode' => 'sandbox',
//        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            'viewPath' => '@common/mail',
//            'useFileTransport' => false,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => '...',
//                'username' => '...',
//                'password' => '...',
//                'port' => '25'
//            ],
//        ],
    ],
];
