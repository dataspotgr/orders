<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contractor".
 *
 * @property int $id
 * @property string $title
 * @property string $job_description
 * @property string $address
 * @property string $city
 * @property string $postal_code
 * @property string $phone_1
 * @property string $phone_2
 * @property string $vat_id
 * @property string $vat_office
 */
class Contractor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contractor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'job_description', 'address', 'city', 'vat_office'], 'string', 'max' => 255],
            [['postal_code'], 'string', 'max' => 10],
            [['phone_1', 'phone_2'], 'string', 'max' => 20],
            [['vat_id'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Επωνυμία',
            'job_description' => 'Περιγραφή εργασίας',
            'address' => 'Διεύθυνση',
            'city' => 'Πόλη',
            'postal_code' => 'ΤΚ',
            'phone_1' => 'Τηλέφωνο',
            'phone_2' => 'Κινητό',
            'vat_id' => 'ΑΦΜ',
            'vat_office' => 'ΔΟΥ',
        ];
    }
	
	public function getAllContractor() {
		$data = Contractor::find()->select(['id','title'])->all();
		return $data;
	}
}
