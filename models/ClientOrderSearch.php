<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientOrder;

/**
 * ClientOrderSearch represents the model behind the search form of `app\models\ClientOrder`.
 */
class ClientOrderSearch extends ClientOrder
{
    public $client;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'order_number', 'cusine_extra_in_stock_1', 'cusine_extra_in_stock_2', 'cusine_extra_in_stock_3', 'cusine_extra_in_stock_4', 'cusine_extra_in_stock_5', 'cusine_extra_in_stock_6', 'cusine_extra_in_stock_7', 'cusine_extra_in_stock_8', 'cusine_extra_in_stock_9', 'cusine_extra_in_stock_10', 'cusine_extra_in_stock_11', 'cusine_extra_in_stock_12', 'cusine_extra_in_stock_13', 'cusine_extra_in_stock_14', 'cusine_extra_in_stock_15', 'cusine_extra_in_stock_16', 'order_status', 'pagkos_type', 'send_to_factory', 'contractor_id'], 'integer'],
            [['order_date', 'delivery_date', 'payment_way', 'model', 'color', 'height', 'cusine_detail_1', 'cusine_detail_2', 'cusine_detail_3', 'cusine_detail_4', 'cusine_detail_5', 'cusine_detail_6', 'cusine_detail_7', 'cusine_detail_8', 'cusine_detail_9', 'cusine_detail_10', 'cusine_detail_11', 'cusine_detail_12', 'cusine_detail_13', 'cusine_detail_14', 'cusine_detail_15', 'cusine_extra_1', 'cusine_extra_2', 'cusine_extra_3', 'cusine_extra_4', 'cusine_extra_5', 'cusine_extra_6', 'cusine_extra_7', 'cusine_extra_8', 'cusine_extra_9', 'cusine_extra_10', 'cusine_extra_11', 'cusine_extra_12', 'cusine_extra_13', 'cusine_extra_14', 'cusine_extra_15', 'cusine_extra_16', 'cusine_extra_price_1', 'cusine_extra_price_2', 'cusine_extra_price_3', 'cusine_extra_price_4', 'cusine_extra_price_5', 'cusine_extra_price_6', 'cusine_extra_price_7', 'cusine_extra_price_8', 'cusine_extra_price_9', 'cusine_extra_price_10', 'cusine_extra_price_11', 'cusine_extra_price_12', 'cusine_extra_price_13', 'cusine_extra_price_14', 'cusine_extra_price_15', 'cusine_extra_price_16', 'cusine_extra_arrival_1', 'cusine_extra_arrival_2', 'cusine_extra_arrival_3', 'cusine_extra_arrival_4', 'cusine_extra_arrival_5', 'cusine_extra_arrival_6', 'cusine_extra_arrival_7', 'cusine_extra_arrival_8', 'cusine_extra_arrival_9', 'cusine_extra_arrival_10', 'cusine_extra_arrival_11', 'cusine_extra_arrival_12', 'cusine_extra_arrival_13', 'cusine_extra_arrival_14', 'cusine_extra_arrival_15', 'cusine_extra_arrival_16', 'installation_comment', 'how_you_found_us', 'memo', 'extra_conditions', 'final_price_comment', 'epimetritis_name', 'transport_date', 'pagkos_name', 'arrival_from_italy', 'salesman_name', 'created_at', 'updated_at', 'client'], 'safe'],
            [['installation_cost', 'final_price', 'final_price_2', 'final_price_3', 'pagkos_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientOrder::find()->JoinWith('client');
//        $query = ClientOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	        'sort' => [
		        'defaultOrder' => ['order_status' => SORT_ASC]
	        ],
        ]);

        $dataProvider->sort->attributes['client'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['clients.fullname' => SORT_ASC],
            'desc' => ['clients.fullname' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'order_date' => $this->order_date,
            'order_number' => $this->order_number,
            'delivery_date' => $this->delivery_date,
            'cusine_extra_in_stock_1' => $this->cusine_extra_in_stock_1,
            'cusine_extra_in_stock_2' => $this->cusine_extra_in_stock_2,
            'cusine_extra_in_stock_3' => $this->cusine_extra_in_stock_3,
            'cusine_extra_in_stock_4' => $this->cusine_extra_in_stock_4,
            'cusine_extra_in_stock_5' => $this->cusine_extra_in_stock_5,
            'cusine_extra_in_stock_6' => $this->cusine_extra_in_stock_6,
            'cusine_extra_in_stock_7' => $this->cusine_extra_in_stock_7,
            'cusine_extra_in_stock_8' => $this->cusine_extra_in_stock_8,
            'cusine_extra_in_stock_9' => $this->cusine_extra_in_stock_9,
            'cusine_extra_in_stock_10' => $this->cusine_extra_in_stock_10,
            'cusine_extra_in_stock_11' => $this->cusine_extra_in_stock_11,
            'cusine_extra_in_stock_12' => $this->cusine_extra_in_stock_12,
            'cusine_extra_in_stock_13' => $this->cusine_extra_in_stock_13,
            'cusine_extra_in_stock_14' => $this->cusine_extra_in_stock_14,
            'cusine_extra_in_stock_15' => $this->cusine_extra_in_stock_15,
            'cusine_extra_in_stock_16' => $this->cusine_extra_in_stock_16,
            'cusine_extra_arrival_1' => $this->cusine_extra_arrival_1,
            'cusine_extra_arrival_2' => $this->cusine_extra_arrival_2,
            'cusine_extra_arrival_3' => $this->cusine_extra_arrival_3,
            'cusine_extra_arrival_4' => $this->cusine_extra_arrival_4,
            'cusine_extra_arrival_5' => $this->cusine_extra_arrival_5,
            'cusine_extra_arrival_6' => $this->cusine_extra_arrival_6,
            'cusine_extra_arrival_7' => $this->cusine_extra_arrival_7,
            'cusine_extra_arrival_8' => $this->cusine_extra_arrival_8,
            'cusine_extra_arrival_9' => $this->cusine_extra_arrival_9,
            'cusine_extra_arrival_10' => $this->cusine_extra_arrival_10,
            'cusine_extra_arrival_11' => $this->cusine_extra_arrival_11,
            'cusine_extra_arrival_12' => $this->cusine_extra_arrival_12,
            'cusine_extra_arrival_13' => $this->cusine_extra_arrival_13,
            'cusine_extra_arrival_14' => $this->cusine_extra_arrival_14,
            'cusine_extra_arrival_15' => $this->cusine_extra_arrival_15,
            'cusine_extra_arrival_16' => $this->cusine_extra_arrival_16,
            'installation_cost' => $this->installation_cost,
            'final_price' => $this->final_price,
            'final_price_2' => $this->final_price_2,
            'final_price_3' => $this->final_price_3,
            'order_status' => $this->order_status,
            'transport_date' => $this->transport_date,
            'pagkos_price' => $this->pagkos_price,
            'pagkos_type' => $this->pagkos_type,
            'send_to_factory' => $this->send_to_factory,
            'arrival_from_italy' => $this->arrival_from_italy,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'contractor_id' => $this->contractor_id,
        ]);

        $query->andFilterWhere(['like', 'payment_way', $this->payment_way])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'height', $this->height])
            ->andFilterWhere(['like', 'cusine_detail_1', $this->cusine_detail_1])
            ->andFilterWhere(['like', 'cusine_detail_2', $this->cusine_detail_2])
            ->andFilterWhere(['like', 'cusine_detail_3', $this->cusine_detail_3])
            ->andFilterWhere(['like', 'cusine_detail_4', $this->cusine_detail_4])
            ->andFilterWhere(['like', 'cusine_detail_5', $this->cusine_detail_5])
            ->andFilterWhere(['like', 'cusine_detail_6', $this->cusine_detail_6])
            ->andFilterWhere(['like', 'cusine_detail_7', $this->cusine_detail_7])
            ->andFilterWhere(['like', 'cusine_detail_8', $this->cusine_detail_8])
            ->andFilterWhere(['like', 'cusine_detail_9', $this->cusine_detail_9])
            ->andFilterWhere(['like', 'cusine_detail_10', $this->cusine_detail_10])
            ->andFilterWhere(['like', 'cusine_detail_11', $this->cusine_detail_11])
            ->andFilterWhere(['like', 'cusine_detail_12', $this->cusine_detail_12])
            ->andFilterWhere(['like', 'cusine_detail_13', $this->cusine_detail_13])
            ->andFilterWhere(['like', 'cusine_detail_14', $this->cusine_detail_14])
            ->andFilterWhere(['like', 'cusine_detail_15', $this->cusine_detail_15])
            ->andFilterWhere(['like', 'cusine_extra_1', $this->cusine_extra_1])
            ->andFilterWhere(['like', 'cusine_extra_2', $this->cusine_extra_2])
            ->andFilterWhere(['like', 'cusine_extra_3', $this->cusine_extra_3])
            ->andFilterWhere(['like', 'cusine_extra_4', $this->cusine_extra_4])
            ->andFilterWhere(['like', 'cusine_extra_5', $this->cusine_extra_5])
            ->andFilterWhere(['like', 'cusine_extra_6', $this->cusine_extra_6])
            ->andFilterWhere(['like', 'cusine_extra_7', $this->cusine_extra_7])
            ->andFilterWhere(['like', 'cusine_extra_8', $this->cusine_extra_8])
            ->andFilterWhere(['like', 'cusine_extra_9', $this->cusine_extra_9])
            ->andFilterWhere(['like', 'cusine_extra_10', $this->cusine_extra_10])
            ->andFilterWhere(['like', 'cusine_extra_11', $this->cusine_extra_11])
            ->andFilterWhere(['like', 'cusine_extra_12', $this->cusine_extra_12])
            ->andFilterWhere(['like', 'cusine_extra_13', $this->cusine_extra_13])
            ->andFilterWhere(['like', 'cusine_extra_14', $this->cusine_extra_14])
            ->andFilterWhere(['like', 'cusine_extra_15', $this->cusine_extra_15])
            ->andFilterWhere(['like', 'cusine_extra_16', $this->cusine_extra_16])
            ->andFilterWhere(['like', 'cusine_extra_price_1', $this->cusine_extra_price_1])
            ->andFilterWhere(['like', 'cusine_extra_price_2', $this->cusine_extra_price_2])
            ->andFilterWhere(['like', 'cusine_extra_price_3', $this->cusine_extra_price_3])
            ->andFilterWhere(['like', 'cusine_extra_price_4', $this->cusine_extra_price_4])
            ->andFilterWhere(['like', 'cusine_extra_price_5', $this->cusine_extra_price_5])
            ->andFilterWhere(['like', 'cusine_extra_price_6', $this->cusine_extra_price_6])
            ->andFilterWhere(['like', 'cusine_extra_price_7', $this->cusine_extra_price_7])
            ->andFilterWhere(['like', 'cusine_extra_price_8', $this->cusine_extra_price_8])
            ->andFilterWhere(['like', 'cusine_extra_price_9', $this->cusine_extra_price_9])
            ->andFilterWhere(['like', 'cusine_extra_price_10', $this->cusine_extra_price_10])
            ->andFilterWhere(['like', 'cusine_extra_price_11', $this->cusine_extra_price_11])
            ->andFilterWhere(['like', 'cusine_extra_price_12', $this->cusine_extra_price_12])
            ->andFilterWhere(['like', 'cusine_extra_price_13', $this->cusine_extra_price_13])
            ->andFilterWhere(['like', 'cusine_extra_price_14', $this->cusine_extra_price_14])
            ->andFilterWhere(['like', 'cusine_extra_price_15', $this->cusine_extra_price_15])
            ->andFilterWhere(['like', 'cusine_extra_price_16', $this->cusine_extra_price_16])
            ->andFilterWhere(['like', 'installation_comment', $this->installation_comment])
            ->andFilterWhere(['like', 'how_you_found_us', $this->how_you_found_us])
            ->andFilterWhere(['like', 'memo', $this->memo])
            ->andFilterWhere(['like', 'extra_conditions', $this->extra_conditions])
            ->andFilterWhere(['like', 'final_price_comment', $this->final_price_comment])
            ->andFilterWhere(['like', 'epimetritis_name', $this->epimetritis_name])
            ->andFilterWhere(['like', 'pagkos_name', $this->pagkos_name])
            ->andFilterWhere(['like', 'salesman_name', $this->salesman_name])
            ->andFilterWhere(['like', 'fullname', $this->client]);

        return $dataProvider;
    }
}
