<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_order_payment".
 *
 * @property int $id
 * @property int $order_id
 * @property string $trans_date
 * @property string $reason
 * @property float $amount
 * @property string $apodeiksi
 * @property string $created_at
 * @property string $updated_at
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_order_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['trans_date', 'created_at', 'updated_at'], 'safe'],
//            [['trans_date', 'created_at', 'updated_at'], 'date'],
            [['amount'], 'number'],
            [['reason'], 'string', 'max' => 255],
            [['apodeiksi'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'ID παραγγελίας',
            'trans_date' => 'Ημ/νία συναλλαγής',
            'reason' => 'Αιτιολογία',
            'amount' => 'Ποσό* (Καθαρό ποσό χωρίς σύμβολα € πχ. 1000)',
            'apodeiksi' => 'Αρ. Απόδειξης', //deleted - user does not want them
            'created_at' => 'Ημέρα πληρωμής',
            'updated_at' => 'Τελευταία ενημέρωση',
        ];
    }
    public function getPaymentsById($order_id) {
        $data = (new \yii\db\Query())->select(['id','order_id','trans_date','reason','amount','apodeiksi','created_at','updated_at'])->from('client_order_payment')->where(['order_id' => $order_id]);
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }

    public function getClientID($order_id) { //paymentsController
        $data = (new \yii\db\Query())->select(['client_id'])->from('client_order')->where(['id' => $order_id]);
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }

    public function getClientInfo($client_id)
    {
        $data = (new \yii\db\Query())->select(['fullname','email','phone','phone2','mobile','mobile2'])->from('clients')->where(['id' => $client_id]);
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }
}
