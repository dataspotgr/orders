<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $email
 * @property string $fullname
 * @property string $firstname
 * @property string $lastname
 * @property string $phone
 * @property string $mobile
 * @property string $phone2
 * @property string $mobile2
 * @property string $address
 * @property string $address_floor
 * @property string $address_area
 * @property string $town
 * @property string $postal_code
 * @property string $created_at
 * @property string $updated_at
 * @property int $open_order
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname', 'open_order'], 'required'],
            [['open_order'], 'integer'],
            [['created_at', 'updated_at'], 'string'],
            [['email', 'fullname', 'firstname', 'lastname', 'address', 'town'], 'string', 'max' => 255],
            [['phone', 'mobile', 'phone2', 'mobile2', 'address_floor', 'address_area', 'postal_code'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'fullname' => 'Ονοματεπώνυμο',
            'firstname' => 'Όνομα',
            'lastname' => 'Επώνυμο',
            'phone' => 'Τηλέφωνο',
            'mobile' => 'Κινητό',
            'phone2' => 'Τηλέφωνο 2',
            'mobile2' => 'Κινητό 2',
            'address' => 'Διεύθυνση',
            'address_floor' => 'Όροφος',
            'address_area' => 'Περιοχή',
            'town' => 'Πόλη',
            'postal_code' => 'ΤΚ',
            'created_at' => 'Δημιουργήθηκε στις',
            'updated_at' => 'Ενημερώθηκε στις',
            'open_order' => 'Κατάσταση παραγγελίας',
        ];
    }

    public function getFullname($client_id) {
        $data = (new \yii\db\Query())->select(['fullname'])->from('clients')->where(['id' => $client_id]);
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }

//    public function getUser($uid) {
//        $data = Clients::find()->select(['id','fullname'])->where(['id'=> $uid]);
//        return $data;
//    }

    public function getAllUsers() {
        $data = Contractor::find()->select(['id','fullname','mobile','phone','email'])->all();
        return $data;
    }
}
