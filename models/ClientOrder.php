<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_order".
 *
 * @property int $id
 * @property int $client_id
 * @property string $order_date
 * @property int $order_number
 * @property string $delivery_date
 * @property string|null $payment_way
 * @property string|null $model
 * @property string|null $color
 * @property string|null $color_down
 * @property string|null $height
 * @property string|null $cusine_detail_1
 * @property string|null $cusine_detail_2
 * @property string|null $cusine_detail_3
 * @property string|null $cusine_detail_4
 * @property string|null $cusine_detail_5
 * @property string|null $cusine_detail_6
 * @property string|null $cusine_detail_7
 * @property string|null $cusine_detail_8
 * @property string|null $cusine_detail_9
 * @property string|null $cusine_detail_10
 * @property string|null $cusine_detail_11
 * @property string|null $cusine_detail_12
 * @property string|null $cusine_detail_13
 * @property string|null $cusine_detail_14
 * @property string|null $cusine_detail_15
 * @property string|null $cusine_extra_1
 * @property string|null $cusine_extra_2
 * @property string|null $cusine_extra_3
 * @property string|null $cusine_extra_4
 * @property string|null $cusine_extra_5
 * @property string|null $cusine_extra_6
 * @property string|null $cusine_extra_7
 * @property string|null $cusine_extra_8
 * @property string|null $cusine_extra_9
 * @property string|null $cusine_extra_10
 * @property string|null $cusine_extra_11
 * @property string|null $cusine_extra_12
 * @property string $cusine_extra_13
 * @property string $cusine_extra_14
 * @property string $cusine_extra_15
 * @property string $cusine_extra_16
 * @property string|null $cusine_extra_price_1
 * @property string|null $cusine_extra_price_2
 * @property string|null $cusine_extra_price_3
 * @property string|null $cusine_extra_price_4
 * @property string|null $cusine_extra_price_5
 * @property string|null $cusine_extra_price_6
 * @property string|null $cusine_extra_price_7
 * @property string|null $cusine_extra_price_8
 * @property string|null $cusine_extra_price_9
 * @property string|null $cusine_extra_price_10
 * @property string|null $cusine_extra_price_11
 * @property string|null $cusine_extra_price_12
 * @property string $cusine_extra_price_13
 * @property string $cusine_extra_price_14
 * @property string $cusine_extra_price_15
 * @property string $cusine_extra_price_16
 * @property int|null $cusine_extra_in_stock_1
 * @property int|null $cusine_extra_in_stock_2
 * @property int|null $cusine_extra_in_stock_3
 * @property int|null $cusine_extra_in_stock_4
 * @property int|null $cusine_extra_in_stock_5
 * @property int|null $cusine_extra_in_stock_6
 * @property int|null $cusine_extra_in_stock_7
 * @property int|null $cusine_extra_in_stock_8
 * @property int|null $cusine_extra_in_stock_9
 * @property int|null $cusine_extra_in_stock_10
 * @property int|null $cusine_extra_in_stock_11
 * @property int|null $cusine_extra_in_stock_12
 * @property int $cusine_extra_in_stock_13
 * @property int $cusine_extra_in_stock_14
 * @property int $cusine_extra_in_stock_15
 * @property int $cusine_extra_in_stock_16
 * @property string|null $cusine_extra_arrival_1
 * @property string|null $cusine_extra_arrival_2
 * @property string|null $cusine_extra_arrival_3
 * @property string|null $cusine_extra_arrival_4
 * @property string|null $cusine_extra_arrival_5
 * @property string|null $cusine_extra_arrival_6
 * @property string|null $cusine_extra_arrival_7
 * @property string|null $cusine_extra_arrival_8
 * @property string|null $cusine_extra_arrival_9
 * @property string|null $cusine_extra_arrival_10
 * @property string|null $cusine_extra_arrival_11
 * @property string|null $cusine_extra_arrival_12
 * @property string|null $cusine_extra_arrival_13
 * @property string|null $cusine_extra_arrival_14
 * @property string|null $cusine_extra_arrival_15
 * @property string|null $cusine_extra_arrival_16
 * @property string|null $extra_other_1
 * @property string|null $extra_price_other_1
 * @property string|null $extra_in_stock_other_1
 * @property string|null $extra_arrival_other_1
 * @property string|null $extra_other_2
 * @property string|null $extra_price_other_2
 * @property string|null $extra_in_stock_other_2
 * @property string|null $extra_arrival_other_2
 * @property float|null $installation_cost
 * @property string $installation_comment
 * @property string|null $how_you_found_us
 * @property string|null $memo
 * @property string|null $extra_conditions
 * @property float|null $final_price
 * @property float $final_price_2
 * @property float $final_price_3
 * @property string|null $final_price_comment
 * @property int|null $order_status
 * @property string $epimetritis_name
 * @property string $transport_date
 * @property float $pagkos_price
 * @property string|null $pagkos_name
 * @property int $pagkos_type
 * @property int|null $send_to_factory
 * @property string|null $arrival_from_italy
 * @property string $salesman_name
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $contractor_id
 * @property array $contractor_names
 * @property  string|$payment_order
 * @property Clients $client
 */
class ClientOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'order_number', 'cusine_extra_in_stock_1', 'cusine_extra_in_stock_2', 'cusine_extra_in_stock_3', 'cusine_extra_in_stock_4',
                'cusine_extra_in_stock_5', 'cusine_extra_in_stock_6', 'cusine_extra_in_stock_7', 'cusine_extra_in_stock_8', 'cusine_extra_in_stock_9',
                'cusine_extra_in_stock_10', 'cusine_extra_in_stock_11', 'cusine_extra_in_stock_12', 'cusine_extra_in_stock_13', 'cusine_extra_in_stock_14',
                'cusine_extra_in_stock_15', 'cusine_extra_in_stock_16', 'order_status', 'pagkos_type', 'send_to_factory', 'contractor_id'], 'integer'],
            [['order_date','delivery_date','cusine_extra_arrival_1', 'cusine_extra_arrival_2', 'cusine_extra_arrival_3', 'cusine_extra_arrival_4', 'cusine_extra_arrival_5',
                'cusine_extra_arrival_6', 'cusine_extra_arrival_7', 'cusine_extra_arrival_8', 'cusine_extra_arrival_9', 'cusine_extra_arrival_10', 'cusine_extra_arrival_11',
                'cusine_extra_arrival_12', 'cusine_extra_arrival_13', 'cusine_extra_arrival_14', 'cusine_extra_arrival_15', 'cusine_extra_arrival_16',
                'installation_cost', 'final_price','final_price_2','final_price_3','pagkos_price','extra_other_1','extra_price_other_1','extra_in_stock_other_1','extra_arrival_other_1',
                'extra_other_2','extra_price_other_2','extra_in_stock_other_2','extra_arrival_other_2','transport_date','arrival_from_italy', 'created_at', 'updated_at'], 'safe'],
            [['memo'], 'string'],
            [['payment_way', 'model', 'color', 'color_down', 'height', 'cusine_detail_1', 'cusine_detail_2', 'cusine_detail_3', 'cusine_detail_4', 'cusine_detail_5', 'cusine_detail_6', 'cusine_detail_7', 'cusine_detail_8', 'cusine_detail_9', 'cusine_detail_10', 'cusine_detail_11', 'cusine_detail_12', 'cusine_detail_13', 'cusine_detail_14', 'cusine_detail_15', 'cusine_extra_1', 'cusine_extra_2', 'cusine_extra_3', 'cusine_extra_4', 'cusine_extra_5', 'cusine_extra_6', 'cusine_extra_7', 'cusine_extra_8', 'cusine_extra_9', 'cusine_extra_10', 'cusine_extra_11', 'cusine_extra_12', 'cusine_extra_13', 'cusine_extra_14', 'cusine_extra_15', 'cusine_extra_16', 'how_you_found_us', 'extra_conditions', 'epimetritis_name', 'salesman_name'], 'string', 'max' => 255],
            [['cusine_extra_price_1', 'cusine_extra_price_2', 'cusine_extra_price_3', 'cusine_extra_price_4', 'cusine_extra_price_5', 'cusine_extra_price_6', 'cusine_extra_price_7', 'cusine_extra_price_8', 'cusine_extra_price_9', 'cusine_extra_price_10', 'cusine_extra_price_11', 'cusine_extra_price_12', 'cusine_extra_price_13', 'cusine_extra_price_14', 'cusine_extra_price_15', 'cusine_extra_price_16', 'installation_comment'], 'string', 'max' => 40],
            [['final_price_comment', 'pagkos_name'], 'string', 'max' => 100],
            [['order_number'], 'unique'],
        ];
    }
    
    //public function findAllContractor()
    //{
	 //   $query = (new \yii\db\Query())->select(['title'])->from('contractor'); //->where(['id' => 1])
	 //   $command = $query->createCommand();
	 //   $data = $command->queryAll();
	 //
	 //   return $data;
    //}
	
	/**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
	        'payment_order'  => 'Πληρωμή',
            'client_id' => 'ID Πελάτη', //Client ID
            'order_date' => 'Ημερομηνία παραγγελίας', // (Order Date)
            'order_number' => 'Αρ. παραγγελίας',
            'delivery_date' => 'Ημερομηνία παράδοσης',
            'payment_way' => 'Τρόπος πληρωμής', // (Payment Way)
            'model' => 'Μοντέλο',
            'color' => 'Χρώμα (πάνω)',
            'color_down' => 'Χρώμα (κάτω)',
            'height' => 'Ύψος',
            'cusine_detail_1' => 'Πάγκος',
            'cusine_detail_2' => 'Πόμολα',
            'cusine_detail_3' => 'Γωνιακός Μηχ.',
            'cusine_detail_4' => 'Κάδος απορημάτων',
            'cusine_detail_5' => 'Μπάζο',
            'cusine_detail_6' => 'Κουταλοθήκη',
            'cusine_detail_7' => 'Χρώμα κασσώματος',
            'cusine_detail_8' => 'Πιατοθήκη',
            'cusine_detail_9' => 'Κορνίζα',
            'cusine_detail_10' => 'Χρώμα Συρταριών',
            'cusine_detail_11' => 'Τιμολόγιο',
            'cusine_detail_12' => 'Εσωτ.Βαγονέτα',
            'cusine_detail_13' => 'Βιτρίνες',
            'cusine_detail_14' => '14',
            'cusine_detail_15' => '15',
            'cusine_extra_1' => 'Σποτ-Φωτισμός',
            'cusine_extra_2' => 'Φ.Μικροκυμάτων',
            'cusine_extra_3' => 'Αποροφητήρας',
            'cusine_extra_4' => 'Ηλ.Φούρνος',
            'cusine_extra_5' => 'Εστίες',
            'cusine_extra_6' => 'Ψυγείο',
            'cusine_extra_7' => 'Π.Πιάτων',
            'cusine_extra_8' => 'Π.Ρούχων',
            'cusine_extra_9' => 'Νεροχύτης',
            'cusine_extra_10' => 'Αξεσουάρ Νεροχύτη',
            'cusine_extra_11' => 'Μπαταρία',
            'cusine_extra_12' => 'Τράπεζι',
            'cusine_extra_13' => 'Καρέκλες',
            'cusine_extra_14' => 'Cusine Extra 14',
            'cusine_extra_15' => 'Cusine Extra 15',
            'cusine_extra_16' => 'Cusine Extra 16',
            'cusine_extra_price_1' => 'Αξία',
            'cusine_extra_price_2' => 'Αξία',
            'cusine_extra_price_3' => 'Αξία',
            'cusine_extra_price_4' => 'Αξία',
            'cusine_extra_price_5' => 'Αξία',
            'cusine_extra_price_6' => 'Αξία',
            'cusine_extra_price_7' => 'Αξία',
            'cusine_extra_price_8' => 'Αξία',
            'cusine_extra_price_9' => 'Αξία',
            'cusine_extra_price_10' => 'Αξία',
            'cusine_extra_price_11' => 'Αξία',
            'cusine_extra_price_12' => 'Αξία',
            'cusine_extra_price_13' => 'Αξία',
            'cusine_extra_price_14' => 'Αξία',
            'cusine_extra_price_15' => 'Αξία',
            'cusine_extra_price_16' => 'Αξία',
            'cusine_extra_in_stock_1' => 'Αποθήκη',
            'cusine_extra_in_stock_2' => 'Αποθήκη',
            'cusine_extra_in_stock_3' => 'Αποθήκη',
            'cusine_extra_in_stock_4' => 'Αποθήκη',
            'cusine_extra_in_stock_5' => 'Αποθήκη',
            'cusine_extra_in_stock_6' => 'Αποθήκη',
            'cusine_extra_in_stock_7' => 'Αποθήκη',
            'cusine_extra_in_stock_8' => 'Αποθήκη',
            'cusine_extra_in_stock_9' => 'Αποθήκη',
            'cusine_extra_in_stock_10' => 'Αποθήκη',
            'cusine_extra_in_stock_11' => 'Αποθήκη',
            'cusine_extra_in_stock_12' => 'Αποθήκη',
            'cusine_extra_in_stock_13' => 'Αποθήκη',
            'cusine_extra_in_stock_14' => 'Αποθήκη',
            'cusine_extra_in_stock_15' => 'Αποθήκη',
            'cusine_extra_in_stock_16' => 'Αποθήκη',
            'cusine_extra_arrival_1' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_2' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_3' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_4' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_5' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_6' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_7' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_8' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_9' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_10' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_11' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_12' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_13' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_14' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_15' => 'Ημ. Παραλαβής',
            'cusine_extra_arrival_16' => 'Ημ. Παραλαβής',
            //dataspot new fields
            'extra_other_1'             => 'Άλλη συσκυεή 1',
            'extra_price_other_1'       => 'Αξία άλλη συσκυεή 1',
            'extra_in_stock_other_1'    => 'Αποθήκη Άλλη συσκυεή 1',
            'extra_arrival_other_1'     => 'Ημ. Παραλαβής Άλλη συσκυεή 1',
            'extra_other_2'             => 'Άλλη συσκυεή 2',
            'extra_price_other_2'       => 'Αξία άλλη συσκυεή 2',
            'extra_in_stock_other_2'    => 'Αποθήκη Άλλη συσκυεή 2',
            'extra_arrival_other_2'     => 'Ημ. Παραλαβής Άλλη συσκυεή 2',
            'installation_cost' => 'Αξία τοποθέτησης:',
            'installation_comment'      => '', //Σχόλια εγκατάστασης (Installation Comment)
            'how_you_found_us'          => 'Γνωριμία με την rigas-cucine.gr:',
            'memo'                      => 'Παρατηρήσεις:',
            'extra_conditions'          => 'Extra Conditions',
            'final_price'               => 'Συνολική αξία:',
            'final_price_2'             => 'Αξία επίπλων', //(Final Price 2)
            'final_price_3'             => 'Αξία ηλεκτρικών και παρελκομένων', // (Final Price 3)
            'final_price_comment'       => 'Σχόλια', //(Final Price Comment)
            'order_status'              => 'Κατάσταση παραγγελίας',
            'epimetritis_name'          => 'Επιμετρητής χώρου',
            'transport_date'            => 'Ημερομηνία μεταφοράς', // (Transport Date)
            'pagkos_price'              => 'Τιμή πάγκου:', //Pagkos Price
            'pagkos_name'               => 'Σχόλια παγκου:', //Pagkos Name
            'pagkos_type'               => 'Τύπος πάγκου:',
            'send_to_factory'           => 'Έχει σταλεί στο εργοστάσιο:',
            'arrival_from_italy'        => 'Ημερομηνία φόρτωσης από Ιταλία:',
            'salesman_name'             => 'Πωλητής:',
            'created_at'                => 'Δημιουργήθηκε στις', // (Created At)
            'updated_at'                => 'Ενημερώθηκε στις', //(Updated At)
            'contractor_id'             => 'Εργολάβος',
	        //'contractor_names' => 'Ονόματα',
        ];
    }


    /**
     * Gets query for [[Client]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    public function getClientInfo($client_id)
    {
        $data = (new \yii\db\Query())->select(['fullname','email','phone','phone2','mobile','mobile2'])->from('clients')->where(['id' => $client_id]);
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }

    public function statusOrder() {
        if($this->order_status == 1) {
            return "Κλειστή";
        }
        elseif($this->order_status == 0) {
            return "Ανοιχτή";
        }
        elseif($this->order_status == 2) {
            return "Άκυρη";
        }
    }

    public function getAllUsers($uid) {
//        echo $uid;
        $data = (new \yii\db\Query())->select(['id','fullname','mobile','phone','email'])->from('clients')->where(['id' => $uid]);
        $command = $data->createCommand();
        $results = $command->queryAll();
//        var_dump($results);
        foreach ($results as $single_user) {
            if($single_user['id'] == $uid){
                return $single_user['fullname'];
            }
        }
    }
    
    public function getOrderID($oid) {
	    $data = (new \yii\db\Query())->select(['id','order_number','order_status'])->from('client_order')->where(['id' => $oid]);
	    $command = $data->createCommand();
	    $results = $command->queryAll();
	    //var_dump($results);
	    foreach ($results as $order_payment) {
	    	if($order_payment['order_status'] == 0) {
	    		return '<a class="payment_btn" href="index.php?r=payments%2Fcreate&order_id='.$order_payment['id'].'">Πληρωμή</a>';
		    }
		    elseif($order_payment['order_status'] == 1) {
	    		return '<a class="payment_btn_str_closed" href="javascript:void(0);">Έκλεισε</a>';
		    }
		    else {
			    return '<a class="payment_btn_str_cancel" href="javascript:void(0);">Ακυρώθηκε</a>';
		    }
	    }
    }

    public function getLastOrderNumber() {
        $data = (new \yii\db\Query())->select(['order_number'])->from('client_order')->orderBy('order_number DESC');
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }

    public function getClientID($order_id) { //paymentsController
        $data = (new \yii\db\Query())->select(['client_id'])->from('client_order')->where(['id' => $order_id]);
        $command = $data->createCommand();
        $results = $command->queryAll();
        return $results;
    }
}
