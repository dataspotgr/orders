<?php

namespace app\controllers;

use Yii;
use app\models\Payments;
use app\models\Clients;
use app\models\Contractor;
use app\models\ClientOrder;
use app\models\ClientOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientOrderController implements the CRUD actions for ClientOrder model.
 */
class ClientOrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['create', 'update','index'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all ClientOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataUsers = new Clients();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'getUsers' => $dataUsers,
        ]);
    }

    /**
     * Displays a single ClientOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
    	
    	$findContractor = new Contractor();
        $order_payment_info = new Payments();
        $order_id = $_GET['id'];

        //ds - get client information
        $attrs = new ClientOrder();
        $client_id = $attrs->getClientID($order_id); //return array
            //var_dump($client_id);
            //echo $client_id[0]['client_id'];

        $client_fulname = $attrs->getClientInfo($client_id[0]['client_id']); //return array
//        echo $client_fulname[0]['fullname'];
//        var_dump($client_fulname);


        return $this->render('view', [
            'model'                     => $this->findModel($id),
            'attrs'                     => $attrs->attributeLabels(),
	        'contractor_names'          => $findContractor->getAllContractor(),
	        'payment_order_information' => $order_payment_info->getPaymentsById($order_id),
            'client_info_fullname'      => $client_fulname[0]['fullname'],
        ]);
    }

    /**
     * Creates a new ClientOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClientOrder();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        //echo $_GET['id'];
        if(isset($_GET['id']) && !empty($_GET['id'])) {
            $model->client_id = $_GET['id'];
        }

        $last_order_number = new ClientOrder();
        $order_number_last = $last_order_number->getLastOrderNumber();
        //        var_dump($order_number_last[0]);
        $current_last_order_number = $order_number_last[0]['order_number'];
        $current_last_order_number += 1;
        $model->order_number = $current_last_order_number;
	
	    //ds - get the names of contractors
	    $contractor = new Contractor();
	    $model->contractor_id = $contractor->getAllContractor();
		//var_dump($model);

        //set default values
        $model->created_at = date('Y-m-d');
        $model->updated_at = date('Y-m-d');
        $model->order_date = date('Y-m-d');

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing ClientOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
	
	    //ds - get the names of contractors
	    $contractor = new Contractor();
	    $model->contractor_id = $contractor->getAllContractor();

        //ds - get client information
        $attrs = new ClientOrder();
        $client_id = $attrs->getClientID($id); //return array
        $client_fulname = $attrs->getClientInfo($client_id[0]['client_id']); //return array

        $model->updated_at = date('Y-m-d');

        return $this->render('update', [
            'model' => $model,
            'client_info_fullname' => $client_fulname[0]['fullname'],
        ]);
    }

    /**
     * Deletes an existing ClientOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClientOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
