<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientOrder */

$this->title = 'Ενημνέρωση παραγγελίας πελάτη: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Παραγγελίες Πελάτη', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ενημέρωση';
?>
<div class="client-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
	    'client_info_fullname' => $client_info_fullname,
    ]) ?>

</div>
