<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'order_date') ?>

    <?= $form->field($model, 'order_number') ?>

    <?= $form->field($model, 'delivery_date') ?>

    <?php // echo $form->field($model, 'payment_way') ?>

    <?php // echo $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'height') ?>

    <?php // echo $form->field($model, 'cusine_detail_1') ?>

    <?php // echo $form->field($model, 'cusine_detail_2') ?>

    <?php // echo $form->field($model, 'cusine_detail_3') ?>

    <?php // echo $form->field($model, 'cusine_detail_4') ?>

    <?php // echo $form->field($model, 'cusine_detail_5') ?>

    <?php // echo $form->field($model, 'cusine_detail_6') ?>

    <?php // echo $form->field($model, 'cusine_detail_7') ?>

    <?php // echo $form->field($model, 'cusine_detail_8') ?>

    <?php // echo $form->field($model, 'cusine_detail_9') ?>

    <?php // echo $form->field($model, 'cusine_detail_10') ?>

    <?php // echo $form->field($model, 'cusine_detail_11') ?>

    <?php // echo $form->field($model, 'cusine_detail_12') ?>

    <?php // echo $form->field($model, 'cusine_detail_13') ?>

    <?php // echo $form->field($model, 'cusine_detail_14') ?>

    <?php // echo $form->field($model, 'cusine_detail_15') ?>

    <?php // echo $form->field($model, 'cusine_extra_1') ?>

    <?php // echo $form->field($model, 'cusine_extra_2') ?>

    <?php // echo $form->field($model, 'cusine_extra_3') ?>

    <?php // echo $form->field($model, 'cusine_extra_4') ?>

    <?php // echo $form->field($model, 'cusine_extra_5') ?>

    <?php // echo $form->field($model, 'cusine_extra_6') ?>

    <?php // echo $form->field($model, 'cusine_extra_7') ?>

    <?php // echo $form->field($model, 'cusine_extra_8') ?>

    <?php // echo $form->field($model, 'cusine_extra_9') ?>

    <?php // echo $form->field($model, 'cusine_extra_10') ?>

    <?php // echo $form->field($model, 'cusine_extra_11') ?>

    <?php // echo $form->field($model, 'cusine_extra_12') ?>

    <?php // echo $form->field($model, 'cusine_extra_13') ?>

    <?php // echo $form->field($model, 'cusine_extra_14') ?>

    <?php // echo $form->field($model, 'cusine_extra_15') ?>

    <?php // echo $form->field($model, 'cusine_extra_16') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_1') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_2') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_3') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_4') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_5') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_6') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_7') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_8') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_9') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_10') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_11') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_12') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_13') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_14') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_15') ?>

    <?php // echo $form->field($model, 'cusine_extra_price_16') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_1') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_2') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_3') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_4') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_5') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_6') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_7') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_8') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_9') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_10') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_11') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_12') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_13') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_14') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_15') ?>

    <?php // echo $form->field($model, 'cusine_extra_in_stock_16') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_1') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_2') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_3') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_4') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_5') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_6') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_7') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_8') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_9') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_10') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_11') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_12') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_13') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_14') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_15') ?>

    <?php // echo $form->field($model, 'cusine_extra_arrival_16') ?>

    <?php // echo $form->field($model, 'installation_cost') ?>

    <?php // echo $form->field($model, 'installation_comment') ?>

    <?php // echo $form->field($model, 'how_you_found_us') ?>

    <?php // echo $form->field($model, 'memo') ?>

    <?php // echo $form->field($model, 'extra_conditions') ?>

    <?php // echo $form->field($model, 'final_price') ?>

    <?php // echo $form->field($model, 'final_price_2') ?>

    <?php // echo $form->field($model, 'final_price_3') ?>

    <?php // echo $form->field($model, 'final_price_comment') ?>

    <?php // echo $form->field($model, 'order_status') ?>

    <?php // echo $form->field($model, 'epimetritis_name') ?>

    <?php // echo $form->field($model, 'transport_date') ?>

    <?php // echo $form->field($model, 'pagkos_price') ?>

    <?php // echo $form->field($model, 'pagkos_name') ?>

    <?php // echo $form->field($model, 'pagkos_type') ?>

    <?php // echo $form->field($model, 'send_to_factory') ?>

    <?php // echo $form->field($model, 'arrival_from_italy') ?>

    <?php // echo $form->field($model, 'salesman_name') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'contractor_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
