<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    echo '<h3><strong>Ονοματεπώνυμο πελάτη:</strong> '. $client_info_fullname . '</h3>';
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'client_id')->textInput()->label('ID Πελάτη <span class="ds-required-field">(Συμπληρώνεται αυτόματα)</span>') //['disabled' => true] ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'order_number')->textInput()->label('Αρ. παραγγελίας <span class="ds-required-field">(Συμπληρώνεται αυτόματα)</span>') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'order_date')->input('date')->label('Ημερομηνία παραγγελίας<span class="ds-required-field">*</span>') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'delivery_date')->input('date') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'send_to_factory')->dropDownList(['0' => 'Όχι', '1' => 'Ναι']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'arrival_from_italy')->input('date') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'payment_way')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'color_down')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'height')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"><h3>Πληροφορίες Κουζίνας</h3></div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_1')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_2')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_3')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_4')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_5')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_6')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_7')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_8')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_9')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_10')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_11')->dropDownList(['0' => 'Όχι' , '1' => 'Ναι']); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_12')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_13')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_14')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_detail_15')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"><h3>Έξτρα Κουζίνας</h3></div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_1',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_1')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_1')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_1')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_2',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_2')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_2')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_2')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_3',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_3')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_3')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_3')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_4',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_4')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_4')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_4')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_5',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_5')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_5')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_5')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_6',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_6')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_6')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_6')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_7',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_7')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_7')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_7')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_8',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_8')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_8')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_8')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_9',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_9')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_9')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_9')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_10',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_10')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_10')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_10')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_11',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_11')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_11')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_11')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_12',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_12')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_12')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_12')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'cusine_extra_13',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_price_13')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_in_stock_13')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cusine_extra_arrival_13')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'extra_other_1',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'extra_price_other_1')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'extra_in_stock_other_1')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'extra_arrival_other_1')->input('date') ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <?= $form->field($model, 'extra_other_2',['labelOptions' => [ 'class' => 'control-label ds-labels-first']])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'extra_price_other_2')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'extra_in_stock_other_2')->dropDownList(['0' => 'Όχι', '1' => 'Παραγγέλθηκε', '2' => 'Υπάρχει']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'extra_arrival_other_2')->input('date') ?>
            </div>
        </div>

<!--        <div class="row">-->
<!--            <div class="col-md-12"><h3>Cucine Extra Price</h3></div>-->
<!--        </div>-->
<!---->
<!--        <div class="row">-->
<!--            <div class="col-md-12"><h3>Cucine Extra in Stock</h3></div>-->
<!--        </div>-->
<!---->
<!--        <div class="row">-->
<!--            <div class="col-md-12"><h3>Cucine Extra in Stock</h3></div>-->
<!--        </div>-->

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'installation_cost')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'installation_comment')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'how_you_found_us')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'memo')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'extra_conditions')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'final_price')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'final_price_2')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'final_price_3')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'final_price_comment')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'pagkos_price')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'pagkos_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'pagkos_type')->dropDownList([ '2' => '', '0' => 'Τεχνογρανίτης', '1' => 'Corian', '3' => 'Ceramic', '4' => 'Neolith', '5' => 'Μάρμαρο', '6' => 'Βακελίτης']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
<!--                --><?//= $form->field($model, 'transport_date')->input('date')->label('Ημερομηνία μεταφοράς<span class="ds-required-field">*</span>') ?>
                <?= $form->field($model, 'transport_date')->input('date') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'created_at')->input('date') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'updated_at')->input('date') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'epimetritis_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'salesman_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group field-clientorder-contractor_id">
                    <label class="control-label" for="clientorder-contractor_id">Εργολάβος:</label>
                    <select id="clientorder-contractor_id" name="ClientOrder[contractor_id]" class="form-control">
                        <option value="0"> -- Επιλογή Εργολάβου -- </option>
                        <?php
                        if(isset($model->contractor_id) && !empty($model->contractor_id)) {
                            //var_dump($model->contractor_id);
                            $contractors_arr = array();
                            foreach ($model->contractor_id as $contractor_name) { ?>
                                <option value="<?php echo $contractor_name['id']; ?>"><?php echo $contractor_name['title']; ?></option>
                            <?php } ?>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
	            <?php
                echo $form->field($model, 'order_status')->dropDownList(['0' => 'Ανοιχτή', '1' => 'Κλειστή', '2' => 'Άκυρη']);
	            ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div> <!-- end container -->
</div>

<script>
//    document.getElementById("clientorder-created_at").defaultValue = "2021-10-13";
</script>
