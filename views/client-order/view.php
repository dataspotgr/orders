<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

//use yii\widgets\ListView;
//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Παραγγελίες πελατών', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-order-view">

    <h1><?= Html::encode($this->title) ?> - <?= $client_info_fullname; ?></h1>
<?php
//var_dump($attrs);
?>
    <p>
        <?= Html::a('Ενημέρωση', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Διαγραφή', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Πληρωμή', ['payments/create', 'order_id' => $model->id], ['class' => 'btn btn-success']) ?>
<!--        --><?//= Html::a('Πληρωμή', ['create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>


    </p>

    <div class="container ds-client-order-view">
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <label>--><?//= $attrs['order_number']; ?><!--</label>-->
<!--                //= $model->client_id; ?>
<!--            </div>-->
<!--        </div>-->

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['order_number']; ?>:</label>
                <?= $model->order_number; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label><?= $attrs['order_date']; ?>:</label>
                <?= $model->order_date; ?>
            </div>
            <div class="col-md-6">
                <label><?= $attrs['delivery_date']; ?>:</label>
                <?= $model->delivery_date; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <label><?= $attrs['send_to_factory']; ?></label>
                <?php
                if($model->send_to_factory == 0) {
                    echo "Όχι";
                }
                else {
                    echo "Ναι";
                }
                ?>
            </div>
            <div class="col-md-6">
                <label><?= $attrs['arrival_from_italy']; ?></label>
                <?= $model->arrival_from_italy; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['payment_way']; ?>:</label>
                <?= $model->payment_way; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['model']; ?>:</label>
                <?= $model->model; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['color']; ?>:</label>
                <?= $model->color; ?><br />
                <label><?= $attrs['color_down']; ?>:</label>
                <?= $model->color_down; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['height']; ?>:</label>
                <?= $model->height; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"><h3>Πληροφορίες Κουζίνας</h3></div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_1']; ?>:</label>
                <?= $model->cusine_detail_1; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_2']; ?>:</label>
                <?= $model->cusine_detail_2; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_3']; ?>:</label>
                <?= $model->cusine_detail_3; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_4']; ?>:</label>
                <?= $model->cusine_detail_4; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_5']; ?>:</label>
                <?= $model->cusine_detail_5; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_6']; ?>:</label>
                <?= $model->cusine_detail_6; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_7']; ?>:</label>
                <?= $model->cusine_detail_7; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_8']; ?>:</label>
                <?= $model->cusine_detail_8; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_9']; ?>:</label>
                <?= $model->cusine_detail_9; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_10']; ?>:</label>
                <?= $model->cusine_detail_10; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_11']; ?>:</label>
                <?= $model->cusine_detail_11; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_12']; ?>:</label>
                <?= $model->cusine_detail_12; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['cusine_detail_13']; ?>:</label>
                <?= $model->cusine_detail_13; ?>
            </div>
<!--            <div class="col-md-4">-->
<!--                <label/= $attrs['cusine_detail_14']; ?><!--:</label>-->
<!--                 $model->cusine_detail_14; ?>
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <label>//= $attrs['cusine_detail_15']; ?><!--:</label>-->
<!--                //= $model->cusine_detail_15; ?>
<!--            </div>-->
        </div>

        <div class="row">
            <div class="col-md-12"><h3>Έξτρα Κουζίνας</h3></div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_1']; ?>:</label>
                <?= $model->cusine_extra_1; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_1']; ?>:</label>
                <?= $model->cusine_extra_price_1; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_1']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_1 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_1 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_1']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_1 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_1;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_2']; ?>:</label>
                <?= $model->cusine_extra_2; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_2']; ?>:</label>
                <?php
                    if($model->cusine_extra_price_2 == '') {
                        echo '-';
                    }
                    else {
                        echo $model->cusine_extra_price_2;
                    }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_2']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_2 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_2 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_2']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_2 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_2;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_3']; ?>:</label>
                <?= $model->cusine_extra_3; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_3']; ?>:</label>
                <?php
                if($model->cusine_extra_price_3 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_3;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_3']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_3 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_3 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_3']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_3 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_3;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_4']; ?>:</label>
                <?= $model->cusine_extra_4; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_4']; ?>:</label>
                <?php
                if($model->cusine_extra_price_4 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_4;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_4']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_4 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_4 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_4']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_4 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_4;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_5']; ?>:</label>
                <?= $model->cusine_extra_5; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_5']; ?>:</label>
                <?php
                if($model->cusine_extra_price_5 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_5;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_5']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_5 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_5 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_5']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_5 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_5;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_6']; ?>:</label>
                <?= $model->cusine_extra_6; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_6']; ?>:</label>
                <?php
                if($model->cusine_extra_price_6 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_6;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_6']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_6 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_6 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_6']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_6 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_6;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_7']; ?>:</label>
                <?= $model->cusine_extra_7; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_7']; ?>:</label>
                <?php
                if($model->cusine_extra_price_7 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_7;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_7']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_7 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_7 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_7']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_7 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_7;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_8']; ?>:</label>
                <?= $model->cusine_extra_8; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_8']; ?>:</label>
                <?php
                if($model->cusine_extra_price_8 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_8;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_8']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_8 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_8 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_8']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_8 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_8;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_9']; ?>:</label>
                <?= $model->cusine_extra_9; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_9']; ?>:</label>
                <?php
                if($model->cusine_extra_price_9 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_9;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_9']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_9 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_9 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_9']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_9 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_9;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_10']; ?>:</label>
                <?= $model->cusine_extra_10; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_10']; ?>:</label>
                <?php
                if($model->cusine_extra_price_10 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_10;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_10']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_10 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_10 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_10']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_10 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_10;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_11']; ?>:</label>
                <?= $model->cusine_extra_11; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_11']; ?>:</label>
                <?php
                if($model->cusine_extra_price_11 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_11;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_11']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_11 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_11 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_11']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_11 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_11;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_12']; ?>:</label>
                <?= $model->cusine_extra_12; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_12']; ?>:</label>
                <?php
                if($model->cusine_extra_price_12 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_12;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_12']; ?>:</label>
                <?php
                if($model->cusine_extra_in_stock_12 == 0) {
                    echo 'Όχι';
                }
                else if($model->cusine_extra_in_stock_12 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_12']; ?>:</label>
                <?php
                if($model->cusine_extra_arrival_12 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_arrival_12;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['cusine_extra_13']; ?>:</label>
                <?= $model->cusine_extra_13; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_price_13']; ?>:</label>
                <?php
                if($model->cusine_extra_price_13 == '') {
                    echo '-';
                }
                else {
                    echo $model->cusine_extra_price_13;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_in_stock_13']; ?>:</label>
                <?php
                    if($model->cusine_extra_in_stock_13 == 0) {
                        echo 'Όχι';
                    }
                    else if($model->cusine_extra_in_stock_13 == 1) {
                        echo 'Παραγγέλθηκε';
                    }
                    else {
                        echo 'Υπάρχει';
                    }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['cusine_extra_arrival_13']; ?>:</label>
                <?php
                    if($model->cusine_extra_arrival_13 == '0000-00-00') {
                        echo '-';
                    }
                    else {
                        echo $model->cusine_extra_arrival_13;
                    }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['extra_other_1']; ?>:</label>
                <?= $model->extra_other_1; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['extra_price_other_1']; ?>:</label>
                <?php
                if($model->extra_price_other_1 == '') {
                    echo '-';
                }
                else {
                    echo $model->extra_price_other_1;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['extra_in_stock_other_1']; ?>:</label>
                <?php
                if($model->extra_in_stock_other_1 == 0) {
                    echo 'Όχι';
                }
                else if($model->extra_in_stock_other_1 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['extra_arrival_other_1']; ?>:</label>
                <?php
                if($model->extra_arrival_other_1 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->extra_arrival_other_1;
                }
                ?>
            </div>
        </div>

        <div class="row ds-labels ds-legend">
            <div class="col-md-12">
                <label><?= $attrs['extra_other_2']; ?>:</label>
                <?= $model->extra_other_2; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['extra_price_other_2']; ?>:</label>
                <?php
                if($model->extra_price_other_2 == '') {
                    echo '-';
                }
                else {
                    echo $model->extra_price_other_2;
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['extra_in_stock_other_2']; ?>:</label>
                <?php
                if($model->extra_in_stock_other_2 == 0) {
                    echo 'Όχι';
                }
                else if($model->extra_in_stock_other_2 == 1) {
                    echo 'Παραγγέλθηκε';
                }
                else {
                    echo 'Υπάρχει';
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['extra_arrival_other_2']; ?>:</label>
                <?php
                if($model->extra_arrival_other_2 == '0000-00-00') {
                    echo '-';
                }
                else {
                    echo $model->extra_arrival_other_2;
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['installation_cost']; ?></label>
                <?= $model->installation_cost . ' ' . $model->installation_comment; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['how_you_found_us']; ?></label>
                <?= $model->how_you_found_us; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['memo']; ?></label>
                <?= $model->memo; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['extra_conditions']; ?>:</label>
                <?= $model->extra_conditions; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['pagkos_type']; ?></label>
                <?php
                if($model->pagkos_type == 0) {
                    echo "Τεχνογρανίτης";
                }
                elseif($model->pagkos_type == 1) {
                    echo "Corian";
                }
                else {
                    echo "";
                }
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['pagkos_price']; ?></label>
                <?php
                    echo $model->pagkos_price;
                ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['pagkos_name']; ?></label>
                <?php
                    echo $model->pagkos_name;
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label><?= $attrs['final_price_2']; ?>:</label>
                <?= $model->final_price_2; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['final_price_3']; ?>:</label>
                <?= $model->final_price_3; ?>
            </div>
            <div class="col-md-4">
                <label><?= $attrs['final_price']; ?></label>
                <?= $model->final_price . " " . $model->final_price_comment; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['epimetritis_name']; ?>:</label>
                <?= $model->epimetritis_name; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['salesman_name']; ?></label>
                <?= $model->salesman_name; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['contractor_id']; ?>:</label>
                <?php
                    $contractor_id = $model->contractor_id;
                    foreach ($contractor_names as $name) {
                    	if($contractor_id == $name['id']) {
                    		echo $name['title'];
	                    }
                    }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label><?= $attrs['order_status']; ?>:</label>
                <?php
                if($model->order_status == 0) {
                    echo "Ανοιχτή";
                }
                else if($model->order_status == 1) {
                    echo "Κλειστή";
                }
                else {
                	echo "Άκυρη";
                }
                ?>
            </div>
        </div>

        <div class="row" style="margin-top:3%;">
            <div class="col-md-12"><h3>Πληρωμές</h3></div>
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Ημέρα</th>
                    <th>Ποσό</th>
                    <th>Αιτιολογία</th>
                    <th>Αρ. Απόδειξης</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                    <tbody>
                    <?php
//                    $count_order_payments = count($payment_order_information);
        //            var_dump($payment_order_information);
                    $total_payments = 0;
                    $temp_total_payments = 0;
                    foreach ($payment_order_information as $payment_order_item) {
                        $total_payments = $total_payments + number_format($payment_order_item['amount'],2,',','.');
                        $temp_total_payments = $temp_total_payments + $payment_order_item['amount'];
                        ?>
                        <tr>
                            <td><?php echo $payment_order_item['trans_date']; ?></td>
                            <td><?php echo number_format($payment_order_item['amount'],2,',','.'); ?></td>
                            <td><?php echo $payment_order_item['reason']; ?></td>
                            <td><?php echo $payment_order_item['apodeiksi']; ?></td>
                            <td>
                                <a href="index.php?r=payments%2Fdelete&amp;id=<?php echo $payment_order_item['id']; ?>" title="Διαγραφή" aria-label="Διαγραφή" data-pjax="0" data-confirm="Είστε σίγουροι για τη διαγραφή του αντικειμένου;" data-method="post">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row" style="margin-top:3%;">
            <div class="col-md-12"><h3>Υπόλοιπα</h3></div>
            <div class="col-md-12">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th style="text-align: right;">Συνολικό ποσό παραγγελίας:</th>
                        <th>
                            <?php
                                echo $final_total_cost = $model->final_price_2 + $model->final_price_3 + $model->pagkos_price;

                            ?>€</th>
                    </tr>
                    <tr>
                        <th style="text-align: right;">Ποσό που έχει πληρωθεί:</th>
                        <th><?= $temp_total_payments ?>€</th>
                    </tr>
                    <tr>
                        <th style="text-align: right;">Υπόλοιπο</th>
                        <th><?= $final_total_cost - $temp_total_payments ?>€</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

<!--        <div class="form-group">-->
<!--            //= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
<!--        </div>-->

<!--        --><?php //ActiveForm::end(); ?>

    </div> <!-- end container -->


    <!--     //= DetailView::widget([
    //        'model' => $model,
    //        'attributes' => [
    //            'id',
    //            'client_id',
    ////            [
    ////                'label'=>'Server',
    ////                'format'=>'raw',
    ////                'value'=>"<button>Server Started</button>",
    ////            ],
    //            'order_date',
    //            'order_number',
    //            'delivery_date',
    //            'payment_way',
    //            'model',
    //            'color',
    //            'height',
    //            'cusine_detail_1',
    //            'cusine_detail_2',
    //            'cusine_detail_3',
    //            'cusine_detail_4',
    //            'cusine_detail_5',
    //            'cusine_detail_6',
    //            'cusine_detail_7',
    //            'cusine_detail_8',
    //            'cusine_detail_9',
    //            'cusine_detail_10',
    //            'cusine_detail_11',
    //            'cusine_detail_12',
    //            'cusine_detail_13',
    //            'cusine_detail_14',
    //            'cusine_detail_15',
    //            'cusine_extra_1',
    //            'cusine_extra_2',
    //            'cusine_extra_3',
    //            'cusine_extra_4',
    //            'cusine_extra_5',
    //            'cusine_extra_6',
    //            'cusine_extra_7',
    //            'cusine_extra_8',
    //            'cusine_extra_9',
    //            'cusine_extra_10',
    //            'cusine_extra_11',
    //            'cusine_extra_12',
    //            'cusine_extra_13',
    //            'cusine_extra_14',
    //            'cusine_extra_15',
    //            'cusine_extra_16',
    //            'cusine_extra_price_1',
    //            'cusine_extra_price_2',
    //            'cusine_extra_price_3',
    //            'cusine_extra_price_4',
    //            'cusine_extra_price_5',
    //            'cusine_extra_price_6',
    //            'cusine_extra_price_7',
    //            'cusine_extra_price_8',
    //            'cusine_extra_price_9',
    //            'cusine_extra_price_10',
    //            'cusine_extra_price_11',
    //            'cusine_extra_price_12',
    //            'cusine_extra_price_13',
    //            'cusine_extra_price_14',
    //            'cusine_extra_price_15',
    //            'cusine_extra_price_16',
    //            'cusine_extra_in_stock_1',
    //            'cusine_extra_in_stock_2',
    //            'cusine_extra_in_stock_3',
    //            'cusine_extra_in_stock_4',
    //            'cusine_extra_in_stock_5',
    //            'cusine_extra_in_stock_6',
    //            'cusine_extra_in_stock_7',
    //            'cusine_extra_in_stock_8',
    //            'cusine_extra_in_stock_9',
    //            'cusine_extra_in_stock_10',
    //            'cusine_extra_in_stock_11',
    //            'cusine_extra_in_stock_12',
    //            'cusine_extra_in_stock_13',
    //            'cusine_extra_in_stock_14',
    //            'cusine_extra_in_stock_15',
    //            'cusine_extra_in_stock_16',
    //            'cusine_extra_arrival_1',
    //            'cusine_extra_arrival_2',
    //            'cusine_extra_arrival_3',
    //            'cusine_extra_arrival_4',
    //            'cusine_extra_arrival_5',
    //            'cusine_extra_arrival_6',
    //            'cusine_extra_arrival_7',
    //            'cusine_extra_arrival_8',
    //            'cusine_extra_arrival_9',
    //            'cusine_extra_arrival_10',
    //            'cusine_extra_arrival_11',
    //            'cusine_extra_arrival_12',
    //            'cusine_extra_arrival_13',
    //            'cusine_extra_arrival_14',
    //            'cusine_extra_arrival_15',
    //            'cusine_extra_arrival_16',
    //            'installation_cost',
    //            'installation_comment',
    //            'how_you_found_us',
    //            'memo:ntext',
    //            'extra_conditions',
    //            'final_price',
    //            'final_price_2',
    //            'final_price_3',
    //            'final_price_comment',
    //            'order_status',
    //            'epimetritis_name',
    //            'transport_date',
    //            'pagkos_price',
    //            'pagkos_name',
    //            'pagkos_type',
    //            'send_to_factory',
    //            'arrival_from_italy',
    //            'salesman_name',
    //            'created_at',
    //            'updated_at',
    //            'contractor_id',
    //        ],
    //    ]);

    ?> -->

</div>
