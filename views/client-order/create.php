<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientOrder */

$this->title = Yii::t('app','Create Client Order');
$this->params['breadcrumbs'][] = ['label' => 'Παραγγελίες Πελάτη', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-order-create">

    <?php
//    if($_GET['id']) {
//    Html::getInputId($model, 'client_id');
//    }
//    ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
