<?php
/* @var $this yii\web\View */
//use rmrevin\yii\fontawesome\FA;
use  yii\helpers\Html;

$this->title = 'Rigas Cucine - Order System';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1><?php echo Yii::t('app','Welcome'); ?></h1>

        <p class="lead">rigas-cucine.gr Order System</p>
    </div>
    <div class="body-content ds-home">
        <div class="row">
            <div class="col-lg-3 text-center">
                <h2>Πελάτες</h2>
                <p><i class="glyphicon glyphicon-user"></i></p>
                <?= Html::a('Πελάτες &raquo;', ['clients/index'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="col-lg-3 text-center">
                <h2>Παραγγελίες</h2>
                <p><i class="glyphicon glyphicon-list-alt"></i></p>
                <?= Html::a('Παραγγελίες &raquo;', ['client-order/index'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="col-lg-3 text-center">
                <h2>Εργολάβοι</h2>
                <p><i class="glyphicon glyphicon-tower"></i></p>
                <?= Html::a('Εργολάβοι &raquo;', ['contractors/index'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="col-lg-3 text-center">
                <h2>Analytics</h2>
                <p><i class="glyphicon glyphicon-tasks"></i></p>
                <p><a class="btn btn-success" href="#">Analytics (Coming soon) &raquo;</a></p>
            </div>
        </div>
    </div>
</div>
