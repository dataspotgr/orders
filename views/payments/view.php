<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Πληρωμές', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Ενημέρωση', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Διαγραφή', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Είστε σίγουρος ότι θέλετε να διαγράψετε αυτή την πληρωμή;',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Επιστροφή στον πελάτη', ['client-order/view', 'id' => $model->order_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'trans_date',
            'reason',
            [
                'attribute' => 'amount',
                'value' => number_format($model->amount,2,',','.'),
            ],
//            'amount',
            'apodeiksi',
            //'created_at',
            //'updated_at',
        ],
    ]) ?>

</div>
