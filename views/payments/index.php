<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentsSeach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Πληρωμές';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Δημιουργία ππληρωμής', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'], //add A/A in first column
            //'id',
            'order_id',
            'trans_date',
            'reason',
            [
                'attribute' => 'amount',
                'value' => function($model) {return number_format($model->amount,2,',','.'); },
            ],
            //'apodeiksi',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
