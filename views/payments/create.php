<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = 'Δημιουργία πληρωμής';
$this->params['breadcrumbs'][] = ['label' => 'Πληρωμές', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
	    'fullname'  => $client_info_fullname,
    ]) ?>

</div>
