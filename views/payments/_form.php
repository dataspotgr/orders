<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                echo '<h3><strong>Ονοματεπώνυμο πελάτη:</strong> '. $fullname . '</h3>';
                ?>
            </div>
        </div>

		<div class="row">
			<div class="col-md-12">
				<?= $form->field($model, 'order_id')->textInput() ?>
			</div>
		</div>

<!--        <div class="row">-->
<!--			<div class="col-md-12">-->
<!--                --><?php //echo $fullname; ?>
<!--			</div>-->
<!--		</div>-->
		
		<div class="row">
			<div class="col-md-12">
				<?=  $form->field($model, 'trans_date')->input('date') ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?= $form->field($model, 'reason')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?= $form->field($model, 'amount')->textInput() ?>
			</div>
		</div>
		
<!--		<div class="row">-->
<!--			<div class="col-md-12">-->
<!--				--><?//= $form->field($model, 'apodeiksi')->textInput(['maxlength' => true]) ?>
<!--			</div>-->
<!--		</div>-->
		
		<!--<div class="row">-->
		<!--	<div class="col-md-12">-->
		<!--		--><?//= $form->field($model, 'created_at')->input('date') ?>
		<!--	</div>-->
		<!--</div>-->
		<!---->
		<!--<div class="row">-->
		<!--	<div class="col-md-12">-->
		<!--		--><?//= $form->field($model, 'updated_at')->input('date') ?>
		<!--	</div>-->
		<!--</div>-->
		
	</div>
	
    <div class="form-group">
        <?= Html::submitButton('Αποθήκευση', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
