<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Πελάτες';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a('Νέος Πελάτης', ['create'], ['class' => 'btn btn-success']) ?></p>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'], //add A/A in first column
            'fullname',
            //'firstname',
            //'lastname',
            'phone',
            'mobile',
            //'phone2',
            //'mobile2',
            'address',
            //'address_floor',
            //'address_area',
            //'town',
            //'postal_code',
            //'created_at',
            //'updated_at',
//            'email:email',
//            'open_order',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>