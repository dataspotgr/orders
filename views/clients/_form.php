<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

<!--    --><?//= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
<!---->
<!--    --><?//= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_floor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'town')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->input('date') ?>
<!--    $form->field($model, 'created_at')->textInput(['maxlength' => true]) -->

    <?= $form->field($model, 'updated_at')->input('date') ?>
<!--  $form->field($model, 'updated_at')->textInput(['maxlength' => true]) -->
	
	<?php
	echo $form->field($model, 'open_order')->dropDownList(['0' => 'Ανοιχτή', '1' => 'Κλειστή', '2' => 'Άκυρη']);
	?>

    <div class="form-group">
        <?= Html::submitButton('Αποθήκευση', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
