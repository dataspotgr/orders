<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Πελάτες', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clients-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
//        var_dump($_GET);
//        echo $_GET['id'];
        ?>
        <?= Html::a('Νέα παραγγελία', ['client-order/create', 'id' => $_GET['id']], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Ενημέρωση πελάτη', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Διαγραφή πελάτη', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Είστε σίγουροι ότι θέλετε να διαγράψετε αυτόν τον πελάτη;',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            'fullname',
//            'firstname',
//            'lastname',
            'phone',
            'mobile',
            'phone2',
            'mobile2',
            'address',
            'address_floor',
            'address_area',
            'town',
            'postal_code',
            'created_at',
            'updated_at',
//            array(
//                'open_order',
//                'value' => (($model->open_order == 0) ? "Κλειστή": "Ανοιχτή"),
//                'attribute'=>'Κατάσταση παραγγελίας',
//            ),
        ],
    ]) ?>

</div>
